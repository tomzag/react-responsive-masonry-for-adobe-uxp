"use strict";

exports.__esModule = true;
exports.ResponsiveMasonry = undefined;

var _Masonry = require("./Masonry");

var _Masonry2 = _interopRequireDefault(_Masonry);

var _ResponsiveMasonry = require("./ResponsiveMasonry");

var _ResponsiveMasonry2 = _interopRequireDefault(_ResponsiveMasonry);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Masonry2.default;
exports.ResponsiveMasonry = _ResponsiveMasonry2.default;