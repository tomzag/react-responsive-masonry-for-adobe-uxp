"use strict";

exports.__esModule = true;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DEFAULT_COLUMNS_COUNT = 1;

var MasonryResponsive = function (_React$Component) {
  _inherits(MasonryResponsive, _React$Component);

  function MasonryResponsive(props) {
    _classCallCheck(this, MasonryResponsive);

    var _this = _possibleConstructorReturn(this, _React$Component.call(this, props));

    _this.state = {
      columnsCount: DEFAULT_COLUMNS_COUNT
    };

    _this.handleResize = _this.handleResize.bind(_this);
    _this.handleRef = _this.handleRef.bind(_this);
    return _this;
  }

  MasonryResponsive.prototype.componentDidMount = function componentDidMount() {
    this.updateColumnsCount();
    window.addEventListener("resize", this.handleResize); // eslint-disable-line
  };

  MasonryResponsive.prototype.componentWillUnmount = function componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize); // eslint-disable-line
  };

  MasonryResponsive.prototype.getSortedBreakPoints = function getSortedBreakPoints() {
    var breakPoints = Object.keys(this.props.columnsCountBreakPoints);
    return breakPoints.sort(function (a, b) {
      return a - b;
    });
  };

  MasonryResponsive.prototype.updateColumnsCount = function updateColumnsCount() {
    var columnsCountBreakPoints = this.props.columnsCountBreakPoints;

    var containerWidth = this.container.offsetWidth;
    var breakPoints = this.getSortedBreakPoints();
    var columnsCount = breakPoints.length > 0 ? columnsCountBreakPoints[breakPoints[0]] : DEFAULT_COLUMNS_COUNT;

    breakPoints.forEach(function (breakPoint) {
      if (breakPoint < containerWidth) {
        columnsCount = columnsCountBreakPoints[breakPoint];
      }
    });

    if (columnsCount && columnsCount !== this.state.columnsCount) {
      this.setState({ columnsCount: columnsCount });
    }
  };

  MasonryResponsive.prototype.handleResize = function handleResize() {
    this.updateColumnsCount();
  };

  MasonryResponsive.prototype.handleRef = function handleRef(ref) {
    if (!this.container) this.container = ref;
  };

  MasonryResponsive.prototype.render = function render() {
    var columnsCount = this.state.columnsCount;
    var _props = this.props,
        children = _props.children,
        className = _props.className,
        style = _props.style;

    return _react2.default.createElement(
      "div",
      { ref: this.handleRef, className: className, style: style },
      _react2.default.Children.map(children, function (child, index) {
        return _react2.default.cloneElement(child, {
          key: index,
          columnsCount: columnsCount
        });
      })
    );
  };

  return MasonryResponsive;
}(_react2.default.Component);

MasonryResponsive.propTypes = process.env.NODE_ENV !== "production" ? {
  children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node]).isRequired,
  columnsCountBreakPoints: _propTypes2.default.object,
  className: _propTypes2.default.string,
  style: _propTypes2.default.object
} : {};

MasonryResponsive.defaultProps = {
  columnsCountBreakPoints: {
    350: 1,
    750: 2,
    900: 3
  },
  className: null,
  style: null
};

exports.default = MasonryResponsive;
module.exports = exports["default"];