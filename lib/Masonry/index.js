"use strict";

exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var styles = {
  container: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "stretch",
    boxSizing: "border-box",
    width: "100%"
  },
  column: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignContent: "stretch",
    flexGrow: 1,
    flexBasis: 0
  }
};

var Masonry = function (_React$Component) {
  _inherits(Masonry, _React$Component);

  function Masonry() {
    _classCallCheck(this, Masonry);

    return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
  }

  Masonry.prototype.getColumns = function getColumns() {
    var _props = this.props,
        children = _props.children,
        columnsCount = _props.columnsCount;

    var columns = Array.from({ length: columnsCount }, function () {
      return [];
    });

    _react2.default.Children.forEach(children, function (child, index) {
      columns[index % columnsCount].push(child);
    });

    return columns;
  };

  Masonry.prototype.renderColumn = function renderColumn(column) {
    var gutter = this.props.gutter;

    return column.map(function (item, i) {
      return _react2.default.createElement(
        "div",
        { key: i, style: { margin: gutter } },
        item
      );
    });
  };

  Masonry.prototype.renderColumns = function renderColumns() {
    var _this2 = this;

    return this.getColumns().map(function (column, i) {
      return _react2.default.createElement(
        "div",
        { key: i, style: styles.column },
        _this2.renderColumn(column)
      );
    });
  };

  Masonry.prototype.render = function render() {
    var _props2 = this.props,
        className = _props2.className,
        style = _props2.style;

    return _react2.default.createElement(
      "div",
      { style: _extends({}, styles.container, style), className: className },
      this.renderColumns()
    );
  };

  return Masonry;
}(_react2.default.Component);

Masonry.propTypes = process.env.NODE_ENV !== "production" ? {
  children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node]).isRequired,
  columnsCount: _propTypes2.default.number,
  gutter: _propTypes2.default.string,
  className: _propTypes2.default.string,
  style: _propTypes2.default.object
} : {};

Masonry.defaultProps = {
  columnsCount: 3,
  gutter: "0",
  className: null,
  style: {}
};

exports.default = Masonry;
module.exports = exports["default"];