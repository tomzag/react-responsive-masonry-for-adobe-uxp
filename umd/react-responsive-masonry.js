/*!
 * react-responsive-masonry v2.0.0 - https://github.com/xuopled/react-responsive-masonry#readme
 * MIT Licensed
 */
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("react"), require("prop-types"));
	else if(typeof define === 'function' && define.amd)
		define(["react", "prop-types"], factory);
	else if(typeof exports === 'object')
		exports["ReactResponsiveMasonry"] = factory(require("react"), require("prop-types"));
	else
		root["ReactResponsiveMasonry"] = factory(root["React"], root["PropTypes"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_1__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(3);


/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Masonry__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ResponsiveMasonry__ = __webpack_require__(5);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "ResponsiveMasonry", function() { return __WEBPACK_IMPORTED_MODULE_1__ResponsiveMasonry__["a"]; });



/* harmony default export */ __webpack_exports__["default"] = (__WEBPACK_IMPORTED_MODULE_0__Masonry__["a" /* default */]);


/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




var styles = {
  container: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "stretch",
    boxSizing: "border-box",
    width: "100%"
  },
  column: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignContent: "stretch",
    flexGrow: 1,
    flexBasis: 0
  }
};

var Masonry = function (_React$Component) {
  _inherits(Masonry, _React$Component);

  function Masonry() {
    _classCallCheck(this, Masonry);

    return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
  }

  Masonry.prototype.getColumns = function getColumns() {
    var _props = this.props,
        children = _props.children,
        columnsCount = _props.columnsCount;

    var columns = Array.from({ length: columnsCount }, function () {
      return [];
    });

    __WEBPACK_IMPORTED_MODULE_0_react___default.a.Children.forEach(children, function (child, index) {
      columns[index % columnsCount].push(child);
    });

    return columns;
  };

  Masonry.prototype.renderColumn = function renderColumn(column) {
    var gutter = this.props.gutter;

    return column.map(function (item, i) {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        "div",
        { key: i, style: { margin: gutter } },
        item
      );
    });
  };

  Masonry.prototype.renderColumns = function renderColumns() {
    var _this2 = this;

    return this.getColumns().map(function (column, i) {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        "div",
        { key: i, style: styles.column },
        _this2.renderColumn(column)
      );
    });
  };

  Masonry.prototype.render = function render() {
    var _props2 = this.props,
        className = _props2.className,
        style = _props2.style;

    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      "div",
      { style: _extends({}, styles.container, style), className: className },
      this.renderColumns()
    );
  };

  return Masonry;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

Masonry.propTypes = {
  children: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.oneOfType([__WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.arrayOf(__WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.node), __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.node]).isRequired,
  columnsCount: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.number,
  gutter: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.string,
  className: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.string,
  style: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.object
};

Masonry.defaultProps = {
  columnsCount: 3,
  gutter: "0",
  className: null,
  style: {}
};

/* harmony default export */ __webpack_exports__["a"] = (Masonry);

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




var DEFAULT_COLUMNS_COUNT = 1;

var MasonryResponsive = function (_React$Component) {
  _inherits(MasonryResponsive, _React$Component);

  function MasonryResponsive(props) {
    _classCallCheck(this, MasonryResponsive);

    var _this = _possibleConstructorReturn(this, _React$Component.call(this, props));

    _this.state = {
      columnsCount: DEFAULT_COLUMNS_COUNT
    };

    _this.handleResize = _this.handleResize.bind(_this);
    _this.handleRef = _this.handleRef.bind(_this);
    return _this;
  }

  MasonryResponsive.prototype.componentDidMount = function componentDidMount() {
    this.updateColumnsCount();
    window.addEventListener("resize", this.handleResize); // eslint-disable-line
  };

  MasonryResponsive.prototype.componentWillUnmount = function componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize); // eslint-disable-line
  };

  MasonryResponsive.prototype.getSortedBreakPoints = function getSortedBreakPoints() {
    var breakPoints = Object.keys(this.props.columnsCountBreakPoints);
    return breakPoints.sort(function (a, b) {
      return a - b;
    });
  };

  MasonryResponsive.prototype.updateColumnsCount = function updateColumnsCount() {
    var columnsCountBreakPoints = this.props.columnsCountBreakPoints;

    var containerWidth = this.container.offsetWidth;
    var breakPoints = this.getSortedBreakPoints();
    var columnsCount = breakPoints.length > 0 ? columnsCountBreakPoints[breakPoints[0]] : DEFAULT_COLUMNS_COUNT;

    breakPoints.forEach(function (breakPoint) {
      if (breakPoint < containerWidth) {
        columnsCount = columnsCountBreakPoints[breakPoint];
      }
    });

    if (columnsCount && columnsCount !== this.state.columnsCount) {
      this.setState({ columnsCount: columnsCount });
    }
  };

  MasonryResponsive.prototype.handleResize = function handleResize() {
    this.updateColumnsCount();
  };

  MasonryResponsive.prototype.handleRef = function handleRef(ref) {
    if (!this.container) this.container = ref;
  };

  MasonryResponsive.prototype.render = function render() {
    var columnsCount = this.state.columnsCount;
    var _props = this.props,
        children = _props.children,
        className = _props.className,
        style = _props.style;

    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      "div",
      { ref: this.handleRef, className: className, style: style },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.Children.map(children, function (child, index) {
        return __WEBPACK_IMPORTED_MODULE_0_react___default.a.cloneElement(child, {
          key: index,
          columnsCount: columnsCount
        });
      })
    );
  };

  return MasonryResponsive;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

MasonryResponsive.propTypes = {
  children: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.oneOfType([__WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.arrayOf(__WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.node), __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.node]).isRequired,
  columnsCountBreakPoints: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.object,
  className: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.string,
  style: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.object
};

MasonryResponsive.defaultProps = {
  columnsCountBreakPoints: {
    350: 1,
    750: 2,
    900: 3
  },
  className: null,
  style: null
};

/* harmony default export */ __webpack_exports__["a"] = (MasonryResponsive);

/***/ })
/******/ ])["default"];
});