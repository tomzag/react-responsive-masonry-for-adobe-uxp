var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React from "react";
import PropTypes from "prop-types";

var styles = {
  container: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "stretch",
    boxSizing: "border-box",
    width: "100%"
  },
  column: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignContent: "stretch",
    flexGrow: 1,
    flexBasis: 0,
    overflowX: "hidden"
  }
};

var Masonry = function (_React$Component) {
  _inherits(Masonry, _React$Component);

  function Masonry() {
    _classCallCheck(this, Masonry);

    return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
  }

  Masonry.prototype.getColumns = function getColumns() {
    var _props = this.props,
        children = _props.children,
        columnsCount = _props.columnsCount;

    var columns = Array.from({ length: columnsCount }, function () {
      return [];
    });

    React.Children.forEach(children, function (child, index) {
      columns[index % columnsCount].push(child);
    });

    return columns;
  };

  Masonry.prototype.renderColumn = function renderColumn(column) {
    var gutter = this.props.gutter;

    return column.map(function (item, i) {
      return React.createElement(
        "div",
        { key: i, style: { margin: gutter } },
        item
      );
    });
  };

  Masonry.prototype.renderColumns = function renderColumns() {
    var _this2 = this;

    return this.getColumns().map(function (column, i) {
      return React.createElement(
        "div",
        { key: i, style: styles.column },
        _this2.renderColumn(column)
      );
    });
  };

  Masonry.prototype.render = function render() {
    var _props2 = this.props,
        className = _props2.className,
        style = _props2.style;

    return React.createElement(
      "div",
      { style: _extends({}, styles.container, style), className: className },
      this.renderColumns()
    );
  };

  return Masonry;
}(React.Component);

Masonry.propTypes = process.env.NODE_ENV !== "production" ? {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
  columnsCount: PropTypes.number,
  gutter: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.object
} : {};

Masonry.defaultProps = {
  columnsCount: 3,
  gutter: "0",
  className: null,
  style: {}
};

export default Masonry;